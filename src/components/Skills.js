import React from 'react';


class SkillsPage extends React.Component {

    render() {
        return (
            <section id="foot">

                <div className="container text-header-foot">
                    <div className="row">
                        <div className="col align-self-center  justify-content-center App pt-4">
                            <h5 className="font-color-fourth gray-text">Latering services in New York</h5>
                            <h2 className="font-color-fourth blue-text">We specialize in corporate and private events</h2>
                            <h6 className="font-color-fourth gray-text">With 20 years of experience, we promise the freshest, local ingredients, hand-crafted cooking sprinkled with our unique whimsical elegance and exceptional service.</h6>
                        </div>

                    </div>
                </div>
                <div className="singleLine"></div>
                <div className="singleLineShadow"></div>

                <div className="chart-box mt-5">
                    <div className="pt-2">
                        <p className="text-header-circle">Skills</p>
                        <div class="d-flex align-content-center flex-wrap margin-3">
                            <div className="p-2">
                                <div className="justify-content-center">
                                    <div className="progress-circle over50 p57">
                                        <span>57%</span>
                                        <div className="left-half-clipper">
                                            <div className="first50-bar"></div>
                                            <div className="value-bar"></div>
                                        </div>
                                    </div>
                                    <p className="text-circle">Photography</p>
                                </div>
                            </div>
                            <div className="p-2">
                                <div className="progress-circle over50 p66">
                                    <span>66%</span>
                                    <div className="left-half-clipper">
                                        <div className="first50-bar"></div>
                                        <div className="value-bar"></div>
                                    </div>
                                </div>
                                <p className="text-circle">Marketing</p>
                            </div>
                            <div className="p-2">
                                <div className="progress-circle over50 p74">
                                    <span>74%</span>
                                    <div className="left-half-clipper">
                                        <div className="first50-bar"></div>
                                        <div className="value-bar"></div>
                                    </div>
                                </div>
                                <p className="text-circle">PhP</p>
                            </div>

                            <div className="p-2">
                                <div className="progress-circle p31">
                                    <span>31%</span>
                                    <div className="left-half-clipper">
                                        <div className="first50-bar"></div>
                                        <div className="value-bar"></div>
                                    </div>
                                </div>
                                <p className="text-circle">3D</p>
                            </div>
                        </div>
                    </div>

                    <div className="pb-5">
                        <div className="container">
                            <div className="row responsive">
                                <div className="col pl-0 pr-5 pt-5">
                                    <div className="">
                                        <div className="row">
                                            <div className="col-10">
                                                <div className="line-progress progress" style={{ "height": "30px" }}>
                                                    <div className="bar-progress progress-bar" style={{ "width": "75%" }}></div>
                                                </div>
                                            </div>
                                            <div className="col-2">
                                                <p className="text-line-right">75%</p>
                                            </div>
                                        </div>
                                        <p className="text-line-left py-0">Creativity</p>
                                    </div>
                                </div>
                                <div className="col pl-0 pr-5 pt-5">
                                    <div>
                                        <div className="row">
                                            <div className="col-10">
                                                <div className="line-progress progress" style={{ "height": "30px" }}>
                                                    <div className="progress-bar bar-progress" style={{ "width": "90%", "height": "30px" }}></div>
                                                </div>
                                            </div>
                                            <div className="col-2">
                                                <p className="text-line-right">90%</p>
                                            </div>
                                        </div>
                                        <p className="text-line-left py-0">PhP</p>
                                    </div>
                                </div>
                                <div className="w-100"></div>
                                <div className="col pl-0 pr-5 pt-5">
                                    <div>
                                        <div className="row">
                                            <div className="col-10">
                                                <div className="line-progress progress" style={{ "height": "30px" }}>
                                                    <div className="progress-bar bar-progress" style={{ "width": "48%", "height": "30px" }}></div>
                                                </div>
                                            </div>
                                            <div className="col-2">
                                                <p className="text-line-right">48%</p>
                                            </div>
                                        </div>
                                        <p className="text-line-left py-2">Cooking</p>
                                    </div>
                                </div>
                                <div className="col pl-0 pr-5 pt-5">
                                    <div>
                                        <div className="row">
                                            <div className="col-10">
                                                <div className="line-progress progress" style={{ "height": "30px" }}>
                                                    <div className="progress-bar bar-progress" style={{ "width": "62%", "height": "30px" }}></div>
                                                </div>
                                            </div>
                                            <div className="col-2">
                                                <p className="text-line-right">62%</p>
                                            </div>
                                        </div>
                                        <p className="text-line-left py-2">Marketing</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </section >


        )
    }
}

export default SkillsPage;
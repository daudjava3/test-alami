import React from 'react';
import { BsChatSquare, BsLifePreserver, BsCameraVideo, BsFileEarmark, BsPeople, BsChevronRight } from "react-icons/bs";
import { FiShoppingCart } from "react-icons/fi";
import WaveSvg from './WaveSvg';
import TooltipComponent from './Tooltip';

// class BodyComponent extends React.Component {
function HomePage(props) {

    // render() {
    return (
        <section id="intro">
            <div className="rightSide">
                <TooltipComponent></TooltipComponent>
            </div>

            <div className="intro-container">

                <div id="introCarousel" className="carousel  slide carousel-fade" data-ride="carousel">

                    <ol className="carousel-indicators"></ol>


                    <div className="carousel-item active">

                        <div className="carousel-container">

                            <div className="carousel-content">
                                <div className="col align-self-center  justify-content-center App">
                                    <h3 className="font-color-fourth white-text">Latering should be an experience</h3>
                                    <h2 className="font-color-fourth white-text">We use only the finest and freshest ingredients</h2>
                                    <h6 className="font-color-fourth white-text">At Sway catering we know that food is an important part of life. <br />
                                                        if the meal is not prefect, your event cannot be perfect</h6>
                                    <div className="">
                                        <a href="#about" className="btn-get-started scrollto">Request a Quote <BsChevronRight /></a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="">This div element has position: relative;
                            <div className="absolute">

                                <div>
                                    <WaveSvg />
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section >


    )
    // }
}

export default HomePage;
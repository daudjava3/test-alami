import React from 'react';

import { OverlayTrigger, Popover, Button } from 'react-bootstrap';
import { BsChatSquare, BsLifePreserver, BsCameraVideo, BsFileEarmark, BsPeople, BsChevronRight } from "react-icons/bs";
import { FiShoppingCart } from "react-icons/fi";

function TooltipComponent(props) {

    const [show, setShow] = React.useState(false);
    const showDropdown = (e) => {
        setShow(!show);

        e.target.style.background = '#b3ffb9';
    }
    const hideDropdown = e => {
        setShow(false);

        e.target.style.background = 'white';
    }
    return (
        <>
            {/* {['BsChatSquare, BsLifePreserver, BsCameraVideo, BsFileEarmark, BsPeople'].map((placement) => (
            ))} */}
            <OverlayTrigger
                trigger="hover"
                key={"left"}
                placement={'left'}
                overlay={
                    <Popover id={`popover-positioned-${'left'}`}>
                        <Popover.Content>
                            <strong className="text-green">{'Presale Chat'}</strong>
                        </Popover.Content>
                    </Popover>
                }
            >
                <div className="iconRight"
                    onMouseEnter={showDropdown}
                    onMouseLeave={hideDropdown}
                >
                    <BsChatSquare />
                </div>
            </OverlayTrigger>
            <OverlayTrigger
                trigger="hover"
                key={"left"}
                placement={'left'}
                overlay={
                    <Popover id={`popover-positioned-${'left'}`}>
                        <Popover.Content>
                            <strong className="text-green">{'Submit Support Ticket'}</strong>
                        </Popover.Content>
                    </Popover>
                }
            >
                <div className="iconRight"
                    onMouseEnter={showDropdown}
                    onMouseLeave={hideDropdown}
                >
                    <BsLifePreserver />
                </div>
            </OverlayTrigger>
            <OverlayTrigger
                trigger="hover"
                key={"left"}
                placement={'left'}
                overlay={
                    <Popover id={`popover-positioned-${'left'}`}>
                        <Popover.Content>
                            <strong className="text-green">{'Read Documentation'}</strong>
                        </Popover.Content>
                    </Popover>
                }
            >
                <div className="iconRight"
                    onMouseEnter={showDropdown}
                    onMouseLeave={hideDropdown}
                >
                    <BsFileEarmark />
                </div>
            </OverlayTrigger>
            <OverlayTrigger
                trigger="click"
                key={"left"}
                placement={'left'}
                overlay={
                    <Popover id={`popover-positioned-${'left'}`}>
                        <Popover.Content>
                            <strong className="text-green">{'Converence Call'}</strong>
                        </Popover.Content>
                    </Popover>
                }
            >
                <div className="iconRight">
                    <BsCameraVideo />
                </div>
            </OverlayTrigger>
            <OverlayTrigger
                trigger="click"
                key={"left"}
                placement={'left'}
                overlay={
                    <Popover id={`popover-positioned-${'left'}`}>
                        <Popover.Content>
                            <strong className="text-green">{'Disscussion'}</strong>
                        </Popover.Content>
                    </Popover>
                }
            >
                <div className="iconRight">
                    <BsPeople />
                </div>
            </OverlayTrigger>
            <OverlayTrigger
                trigger="click"
                key={"left"}
                placement={'left'}
                overlay={
                    <Popover id={`popover-positioned-${'left'}`}>
                        <Popover.Content>
                            <strong className="text-green">{'Your Cart'}</strong>
                        </Popover.Content>
                    </Popover>
                }
            >
                <div className="iconRight">
                    <FiShoppingCart />
                </div>
            </OverlayTrigger>
        </>
    )
}

export default TooltipComponent;
import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    useParams,
} from "react-router-dom";
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from 'react-bootstrap';

import 'bootstrap/dist/css/bootstrap.min.css';
import { BsSearch } from "react-icons/bs";
import { FiShoppingCart } from "react-icons/fi";
// import Home from './Home';
// import AboutUs from './AboutUs';
// import ContactUs from './ContactUs';

function NavbarComponent(props) {


    const [show, setShow] = React.useState(false);
    const showDropdown = (e) => {
        setShow(!show);
    }
    const hideDropdown = e => {
        setShow(false);
    }
    return (
        <div>
            <div className="row m-0">
                <div className="col-md-12">
                    <Router>
                        <Navbar collapseOnSelect expand="lg" bg="gray" variant="light">
                            <Navbar.Brand href="#home"></Navbar.Brand>
                            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                            <Navbar.Collapse id="responsive-navbar-nav">
                                <Nav className="mr-auto pl-5">
                                    <NavDropdown title="Demos" disabled id="collasible-nav-dropdown" />
                                    <NavDropdown title="Pricing" disabled className="blue-text" id="collasible-nav-dropdown" />
                                    <NavDropdown title="Portfolio"
                                        id="collasible-nav-dropdown"
                                        show={show}
                                        onMouseEnter={showDropdown}
                                        onMouseLeave={hideDropdown}
                                    >
                                        <div className="width-35-rem">
                                            <div className="row">
                                                <div className="col-sm pl-5 py-2">
                                                    <div className="pb-2 text-menu-head">Grid minimal</div>
                                                    <div><NavDropdown.Item eventKey="1" href="#action/3.1" className="p-0 text-menu-sub">2 columns</NavDropdown.Item></div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">3 columns</NavDropdown.Item></div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">4 columns</NavDropdown.Item></div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">Full width</NavDropdown.Item></div>
                                                </div>
                                                <div className="col-sm pl-3 py-2">
                                                    <div className="pb-2 text-menu-head">Masonry minimal</div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">2 columns</NavDropdown.Item></div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">3 columns</NavDropdown.Item></div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">4 columns</NavDropdown.Item></div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">Full width</NavDropdown.Item></div>
                                                </div>
                                                <div className="col-sm pl-5 py-2">
                                                    <div className="pb-2 text-menu-head">Grid detailed</div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">2 columns</NavDropdown.Item></div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">3 columns</NavDropdown.Item></div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">4 columns</NavDropdown.Item></div>
                                                    <div><NavDropdown.Item href="#action/3.1" className="p-0 text-menu-sub">Full width</NavDropdown.Item></div>
                                                </div>
                                            </div>
                                        </div>
                                    </NavDropdown>
                                </Nav>

                                <Form inline className="pr-5">
                                    <FiShoppingCart className="mr-3" />
                                    <BsSearch className="mr-3" />
                                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                                </Form>
                            </Navbar.Collapse>
                        </Navbar>
                    </Router>
                </div>
            </div >
        </div >
    )

}

export default NavbarComponent;
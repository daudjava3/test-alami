import logo from './logo.svg';
import './App.css';
import NavbarComponent from './components/Navbar';
import HomePage from './components/Home';
import SkillsPage from './components/Skills';

function App() {
  return (
    <>
      <div className="App">
        <NavbarComponent />
        <HomePage />
        <SkillsPage />
      </div>
    </>
  );
}

export default App;
